using System;

namespace Learning.Expert.Development.OOP.Inheritance
{
    public class Inheritance
    {
        public static void Run()
        {
            var result = DerivedClass.MultiplicationWithSomeSauce();
            Console.WriteLine(result);
        }
    }

    public class BaseClass
    {
        /// <summary>
        /// This functions multiples x * y and with the result of a * b 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>(x * y) * (a * b)</returns>
        protected int Multiplication(int x, int y)
        {
            var results = ((x * y));
            return results;
        }
    }

    public class DerivedClass : BaseClass
    {
        public static int MultiplicationWithSomeSauce()
        {
            const int theSauce = 7 * 7;
            var derivedClass = new DerivedClass();
            var result = derivedClass.Multiplication(theSauce, 5);
            return result;
        }
    }
}