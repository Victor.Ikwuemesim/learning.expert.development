# **Inheritance**
> inheritance allows one class to inherit fields and methods from another class to inherit from another class

> there are two main types of classes. the base class or parent class and the derived class or child class.

> we also refer to the base or parent class as the superclass. we can refer to the derived or child class as the subclass.

> The derived class goes on the left hand side of the colon. the base class goes on the right hand side.

### Look at the code below
``` c#

    using System;
    
    namespace Learning.Expert.Development.ObjectOrientedProgramming
    {
        public class Inheritance
        {
            public static void Run()
            {
                var result = DerivedClass.MultiplicationWithSomeSauce();
                Console.WriteLine(result);
            }
        }
    
        public class BaseClass
        {
            /// <summary>
            /// This functions multiples x * y and with the result of a * b 
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns>(x * y) * (a * b)</returns>
            protected int Multiplication(int x, int y)
            {
                var results = ((x * y));
                return results;
            }
        }
    
        public class DerivedClass : BaseClass
        {
            public static int MultiplicationWithSomeSauce()
            {
                const int theSauce = 7 * 7;
                var derivedClass = new DerivedClass();
                var result = derivedClass.Multiplication(theSauce, 5);
                return result;
            }
        }
    }
```