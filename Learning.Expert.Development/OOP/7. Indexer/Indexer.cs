using System;

namespace Learning.Expert.Development.OOP._7._Indexer
{
    public class Indexer
    {
        public static void Run()
        {
            IndexerClass indexerClass = new IndexerClass();
            indexerClass[0] = "98h71yh2g7";
            indexerClass[1] = false;
            indexerClass[2] = 1.2f;
            indexerClass[3] = "12uy1tt31";

            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(indexerClass[i]);
            }

        }
    }

    public class IndexerClass
    {
        private readonly string[] dataArray = new string[50];
        
        public object this[int index]
        {
            get
            {
                // check that the index is valid
                // check that the index is not greater than or equals to the length of our dataArray[] object
                if (index < 0 && index >= dataArray.Length) 
                {
                    Console.WriteLine("Invalid Index");
                    return new object();
                }
                else
                {
                    return dataArray[index];
                }
                
            }
            set
            {
                // check that the index is valid
                // check that the index is not greater than or equals to the length of our dataArray[] object
                if (index < 0 && index >= dataArray.Length) 
                {
                    Console.WriteLine("Invalid Index");
                }
                else
                {
                    dataArray[index] = value.ToString();
                }
            }
        }
    }
}