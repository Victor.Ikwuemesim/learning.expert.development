# **Indexer**

> indexer is a special type of method that we can add to a class to allow it to be indexed like an array.

> indexes are not defined with names.

> they are defined with the this keyword, which refers to the object instance.

> with an indexer, we use get and set to get or set a particular value from the object instance

> we don't give an indexer or a function name.

> we use the "this" keyword to define the indexer for our class.



### Look at the code below
``` c#

    using System;

    namespace Learning.Expert.Development.OOP._7._Indexer
    {
        public class Indexer
        {
            public static void Run()
            {
                IndexerClass indexerClass = new IndexerClass();
                indexerClass[0] = "98h71yh2g7";
                indexerClass[1] = false;
                indexerClass[2] = 1.2f;
                indexerClass[3] = "12uy1tt31";
    
                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine(indexerClass[i]);
                }
    
            }
        }
    
        public class IndexerClass
        {
            private readonly string[] dataArray = new string[50];
            
            public object this[int index]
            {
                get
                {
                    // check that the index is valid
                    // check that the index is not greater than or equals to the length of our dataArray[] object
                    if (index < 0 && index >= dataArray.Length) 
                    {
                        Console.WriteLine("Invalid Index");
                        return new object();
                    }
                    else
                    {
                        return dataArray[index];
                    }
                    
                }
                set
                {
                    // check that the index is valid
                    // check that the index is not greater than or equals to the length of our dataArray[] object
                    if (index < 0 && index >= dataArray.Length) 
                    {
                        Console.WriteLine("Invalid Index");
                    }
                    else
                    {
                        dataArray[index] = value.ToString();
                    }
                }
            }
        }
    }
```