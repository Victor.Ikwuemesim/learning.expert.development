namespace Learning.Expert.Development.OOP.Abstract
{
    public abstract class Abstract
    {
        /// <summary>
        /// This functions multiples x * y and with the result of a * b 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>(x * y)</returns>
        public int Multiplication(int x, int y)
        {
            var results = ((x * y));
            return results;
        }
    }
    
    public class Abstraction : Abstract
    {
        public static void Run()
        {
            //the below mentioned code will cause a compile error 
            //Abstract @abstract = new Abstract();
            Abstraction abstraction = new Abstraction();
            abstraction.Multiplication(5,5);

        }
    }
}