# **Abstraction**
> it hides the implementation details

> abstraction lets us focus on what an object does instead of how it does it.

> It can be achieved in one of two ways and C#, either by using interfaces or abstract classes.

> an abstract class cannot be instantiated directly.

> you cannot implement a abstract class to a static method 

### Look at the code below
``` c#

    namespace Learning.Expert.Development.ObjectOrientedProgramming
    {
        public abstract class Abstract
        {
            /// <summary>
            /// This functions multiples x * y and with the result of a * b 
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns>(x * y)</returns>
            public int Multiplication(int x, int y)
            {
                var results = ((x * y));
                return results;
            }
        }
        
        public class Abstraction : Abstract
        {
            public static void Run()
            {
                //the below mentioned code will cause a compile error 
                //Abstract @abstract = new Abstract();
                Abstraction abstraction = new Abstraction();
                abstraction.Multiplication(5,5);
    
            }
        }
    }
```