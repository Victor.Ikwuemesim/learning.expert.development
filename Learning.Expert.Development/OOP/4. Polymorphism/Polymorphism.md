# **Polymorphism**
> "poly" means ***many*** : "morphism" means ***forms***

> polymorphism allows us to use the methods inherited from classes to perform different tasks.

> polymorphism and inheritance are closely related.

> to override a base class method. we have to add the "virtual" keyword to the base class method and then the "override" keyword to the derived classes method.

> without it the base class will run method
### Look at the code below
``` c#

    using System;
    
    namespace Learning.Expert.Development.ObjectOrientedProgramming
    {
        public static class Polymorphism
        {
            public static void Run()
            {
                SuperClass theOne = new SubClass();
                theOne.Print();
            }
        }
        public class SuperClass
        {
            public virtual void Print()
            {
                Console.WriteLine("Base Class Multiplication method");
            }
        }
    
        public class SubClass : SuperClass
        {
            public override void Print()
            {
                Console.WriteLine("Derived Class Multiplication method");
            }
        }
    }
```