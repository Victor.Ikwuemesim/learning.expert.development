using System;

namespace Learning.Expert.Development.OOP.Polymorphism
{
    public static class Polymorphism
    {
        public static void Run()
        {
            SuperClass theOne = new SubClass();
            theOne.Print();
        }
    }
    public class SuperClass
    {
        public virtual void Print()
        {
            Console.WriteLine("Base Class Multiplication method");
        }
    }

    public class SubClass : SuperClass
    {
        public override void Print()
        {
            Console.WriteLine("Derived Class Multiplication method");
        }
    }
}