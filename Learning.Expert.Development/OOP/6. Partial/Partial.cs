namespace Learning.Expert.Development.OOP.Partial
{
    public class Partial
    {
        public static void Run()
        {
            BaseClass baseClass = new BaseClass();
            // RunMethodOne() from the BaseClass.cs file
            baseClass.RunMethodOne();
            
            // RunMethodTwo() and RunMethodThree() from the PartialBaseClass.cs file
            baseClass.RunMethodTwo();
            baseClass.RunMethodThree();
        }
    }
}