# **Partial Class**
> a class is contained inside of one C# file. we can also have a class that spans multiple files, in other words, part of the class could be defined in one file and part of the class could be defined

> to achieve this, we can use the partial keyword. the partial keyword can be used to split the definition of a class method interface or struct into multiple files.

> it is important to note that all the partial classes, methods, interfaces are struct must have the same access modifiers.

> we cannot have part of a base class public and part of the same base class private.

> all of our partial class definitions have to live inside of the same namespace. also, we need to ensure that they all have the same accessibility.

> the partial keyword has to come immediately before the class keyword. 

> it gives multiple developers the opportunity to collaborate on writing a single class but each can work a separate file.