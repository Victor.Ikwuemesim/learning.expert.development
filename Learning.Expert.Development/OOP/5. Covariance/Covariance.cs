namespace Learning.Expert.Development.OOP.Covariance
{
    public class Covariance
    {
        public static void Run()
        {
            ParentClass parentClass = new ParentClass();
            Covariance.Executable(parentClass);
            ChildClass childClass = new ChildClass();
            Covariance.Executable(childClass);
        }

        private static void Executable(ParentClass parentClass)
        {
            
        }
    }

    public class ParentClass
    {
        
    }

    public class ChildClass : ParentClass
    {
        
    }
}