# **Covariance**
> any object that is a child class is both its own type and the type of any parent class that inherits from.

> it will allow us to pass into the Executable(ParentClass parentClass) method the ParentClass and ChildClass because ChildClass inherits from the ParentClass

### Look at the code below
``` c#

    namespace Learning.Expert.Development.OOP.Covariance
    {
        public class Covariance
        {
            public static void Run()
            {
                ParentClass parentClass = new ParentClass();
                Covariance.Executable(parentClass);
                ChildClass childClass = new ChildClass();
                Covariance.Executable(childClass);
            }
    
            private static void Executable(ParentClass parentClass)
            {
                
            }
        }
    
        public class ParentClass
        {
            
        }
    
        public class ChildClass : ParentClass
        {
            
        }
    }
```