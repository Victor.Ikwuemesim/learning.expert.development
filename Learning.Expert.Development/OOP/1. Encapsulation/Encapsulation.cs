namespace Learning.Expert.Development.OOP.Encapsulation
{
    public static class Encapsulation
    {
        //static variables must be private because it is only used by the Encapsulation class
        private const int _x = 5;
        private const int _y = 3;
        
        /// <summary>
        /// This functions multiples x * y and with the result of a * b 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>(x * y) * (a * b)</returns>
        private static int Multiplication(int x, int y)
        {
            var results = ((x * y) * (_x * _y));
            return results;
        }
        /// <summary>
        /// Executes the Multiplication() function
        /// </summary>
        public static void Run()
        {
            Encapsulation.Multiplication(23,3);
        }
    }
}