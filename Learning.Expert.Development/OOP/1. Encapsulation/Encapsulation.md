# **Encapsulation**
> means data hiding,this means that other classes will not understand the inner details of the class, which is good.

> reusability is a benefit of encapsulation, as encapsulation makes it easy to modify the code as the requirements change, encapsulation makes testing code easier.

> encapsulated code is very easy to test using unit tests.

> in summary, encapsulation is the principle of hiding the internal state and behaviour of an object.

### Look at the code below
``` c#

    namespace Learning.Expert.Development.ObjectOrientedProgramming
    {
        public class Encapsulation
        {
            //static variables must be private because it is only used by the Encapsulation class
            private const int _x = 5;
            private const int _y = 3;
            
            /// <summary>
            /// This functions multiples x * y and with the result of a * b 
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns>(x * y) * (a * b)</returns>
            public static int Multiplication(int x, int y)
            {
                var results = ((x * y) * (_x * _y));
                return results;
            }
            /// <summary>
            /// Executes the Multiplication() function
            /// </summary>
            public static void Run()
            {
                Encapsulation.Multiplication(23,3);
            }
        }
    }
```