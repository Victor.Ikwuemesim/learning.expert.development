﻿using System;
using Learning.Expert.Development.Basics;
using Learning.Expert.Development.OOP._7._Indexer;
using Learning.Expert.Development.OOP.Encapsulation;
using Learning.Expert.Development.OOP.Polymorphism;

namespace Learning.Expert.Development
{
    class Program
    {
        static void Main(string[] args)
        {
            // IsOperator.Run();
            // AsOperator.Run();
            // TenaryOperator.Run();
            // NullCoalescingOperator.Run();
            // Encapsulation.Run();
            // Polymorphism.Run();
            Indexer.Run();
        }
    }
}