using System;
using System.Xml.Schema;

namespace Learning.Expert.Development.Basics
{
    public static class IsOperator
    {
        public static void Run()
        {
            string textInformation = "I am a string";
            bool isObjectString = textInformation is string;
            Console.WriteLine($"Is Object a string? {isObjectString}");
        }
    }
}