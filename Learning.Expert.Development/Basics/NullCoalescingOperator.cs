namespace Learning.Expert.Development.Basics
{
    public static class NullCoalescingOperator
    {
        public static void Run()
        {
            object firstObject = null;
            object returnedObject = firstObject ?? new object();
        }
    }
}