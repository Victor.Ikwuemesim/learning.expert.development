using System;

namespace Learning.Expert.Development.Basics
{
    public static class TenaryOperator
    {
        public static void Run()
        {
            int x = 5;
            int y = 0;

            bool isConditionTrue = x > y ? true : false;
            
            Console.WriteLine(isConditionTrue);
        }
    }
}