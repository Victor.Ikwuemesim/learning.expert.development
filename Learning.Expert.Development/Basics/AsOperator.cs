using System;

namespace Learning.Expert.Development.Basics
{
    public static class AsOperator
    {
        public static void Run()
        {
            string firstStringObject = "This is the first text";
            object stringObject = firstStringObject;
            string secondStringObject = stringObject as string;
            
            Console.WriteLine(secondStringObject);
        }
    }
}